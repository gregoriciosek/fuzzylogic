import java.util.LinkedHashMap;

public class LinguisticVariableGroup {

    String linguisticVariableGroupName;
    private LinkedHashMap<String, LinguisticVariable> linguisticVariableGroup;

    public LinguisticVariableGroup(String linguisticVariableGroupName) {
        this.linguisticVariableGroupName = linguisticVariableGroupName;
        this.linguisticVariableGroup = new LinkedHashMap<String, LinguisticVariable>();
    }

    public String getLinguisticVariableGroupName() {
        return linguisticVariableGroupName;
    }

    public void setLinguisticVariableGroupName(String linguisticVariableGroupName) {
        this.linguisticVariableGroupName = linguisticVariableGroupName;
    }

    public LinkedHashMap<String, LinguisticVariable> getLinguisticVariableGroup() {
        return linguisticVariableGroup;
    }

    public void setLinguisticVariableGroup(LinkedHashMap<String, LinguisticVariable> linguisticVariableGroup) {
        this.linguisticVariableGroup = linguisticVariableGroup;
    }

    public void addLinguisticVariable(LinguisticVariable linguisticVariable) {
        linguisticVariableGroup.put(linguisticVariable.getLinguisticVariableName(), linguisticVariable);
    }
}
