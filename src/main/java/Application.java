import java.util.Map;

public class Application{

    private static FuzzyLogic fuzzyLogic = new FuzzyLogic();
    private static DataCreator dataCreator = new DataCreator();

    public static void main(String args[]) {

        /*Wartości zmiennych wejściowych ostrych do fuzyfikacji!*/

        float roomTemperature = 20;
        System.out.println("\n\nWartość do rozmycia : " + roomTemperature +".\n\n");

        dataCreator.fillMyFuzzyLogic(fuzzyLogic);

            /* Grupy zmiennych lingwistycznych dla R1 */
            LinguisticVariableGroup linguisticVariableGroupR1Entrances = new LinguisticVariableGroup("R1_Wejścia");
            LinguisticVariableGroup linguisticVariableGroupR1Exits = new LinguisticVariableGroup("R1_Wyjścia");
            dataCreator.createLinguisticVariableGroups_1(fuzzyLogic, linguisticVariableGroupR1Entrances, linguisticVariableGroupR1Exits);
            /* Grupy zmiennych lingwistycznych dla R2 */
            LinguisticVariableGroup linguisticVariableGroupR2Entrances = new LinguisticVariableGroup("R2_Wejścia");
            LinguisticVariableGroup linguisticVariableGroupR2Exits = new LinguisticVariableGroup("R2_Wyjścia");
            dataCreator.createLinguisticVariableGroups_2(fuzzyLogic, linguisticVariableGroupR2Entrances, linguisticVariableGroupR2Exits);
            /* Grupy zmiennych lingwistycznych Agregacji */
            LinguisticVariableGroup linguisticVariableGroupAggregation = new LinguisticVariableGroup("Aggregation");
            dataCreator.createAgregationVariableGroup(fuzzyLogic, linguisticVariableGroupAggregation);
            /* Grupy zmiennych lingwistycznych wyjściowych */
            LinguisticVariableGroup linguisticVariableGroupExits = new LinguisticVariableGroup("Exits");
            dataCreator.createExitsLinguisticVariableGroup(fuzzyLogic, linguisticVariableGroupExits);
            /* ----------------------------------------------- */

        /* Rozmywanie */
        System.out.println("\n**********************");
        System.out.println("Rozmywanie : ");
        System.out.println("**********************");

        fuzzyLogic.fuzzyfication("cieplo", roomTemperature, false);
        fuzzyLogic.fuzzyfication("zimno", roomTemperature, false);
        fuzzyLogic.fuzzyfication("niecieplo", roomTemperature, true);

        /* Wnioskowanie */
        System.out.println("\n**********************");
        System.out.println("Wnioskowanie : ");
        System.out.println("**********************");
        fuzzyLogic.inference(linguisticVariableGroupR1Entrances, linguisticVariableGroupR1Exits);
        fuzzyLogic.inference(linguisticVariableGroupR2Entrances, linguisticVariableGroupR2Exits);

        /* Agregacja */
        System.out.println("\n**********************");
        System.out.println("Agregacja : ");
        System.out.println("**********************");
        fuzzyLogic.aggregation(linguisticVariableGroupAggregation, linguisticVariableGroupExits);

        /* Wyostrzenie metodą środka ciężkości */
        System.out.println("\n**********************");
        System.out.println("Wyostrzanie : ");
        System.out.println("**********************");
        fuzzyLogic.defuzzyficationIteration(fuzzyLogic, linguisticVariableGroupExits, 5, 1, 10);

        }
    }
