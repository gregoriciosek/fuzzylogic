import java.util.LinkedHashMap;
import java.util.Map;

public class FuzzyLogic {

    private Map<String, LinguisticVariable> linguisticVariableMap;

    public FuzzyLogic(){
        linguisticVariableMap = new LinkedHashMap<String, LinguisticVariable>();
    }

    public Map<String, LinguisticVariable> getLinguisticVariableMap() {
        return linguisticVariableMap;
    }

    public void setLinguisticVariableMap(Map<String, LinguisticVariable> linguisticVariableMap) {
        this.linguisticVariableMap = linguisticVariableMap;
    }

    public void addLinguisticVariable(LinguisticVariable linguisticVariable) {
        linguisticVariableMap.put(linguisticVariable.getLinguisticVariableName(), linguisticVariable);
    }

    public float getTermUValue(String termsName, String termName){
        return linguisticVariableMap.get(termsName).getTerms().get(termName).getuValue();
    }

    /* Rozmywanie */

    public void fuzzyfication(String linquisticVariableName, float temperature, boolean reversed) {

        System.out.println("\n" + linquisticVariableName);
        this.getLinguisticVariableMap().get(linquisticVariableName).getTerms().forEach((k, v) -> {
            System.out.print("Wartość dla termu " + v.getTermName() + " : ");
            if(reversed)
                v.setuValue(v.reversedValueFuzzyfication(temperature));
            else
                v.setuValue(v.valueFuzzyfication(temperature));
            System.out.println(v.getuValue());
            if(reversed)
                v.reversedFuzzyfication();
            else
                v.fuzzyfication();
        });
    }

    /*Wnioskowanie*/

    public void inference(LinguisticVariableGroup linguisticVariableGroupEntry, LinguisticVariableGroup linguisticVariableGroupExit) {

        linguisticVariableGroupExit.getLinguisticVariableGroup().forEach((exitKey, exitValue) -> {
            System.out.println("\n" + exitValue.getLinguisticVariableName());
            exitValue.getTerms().forEach((termKey, termValue) -> {
                termValue.setuValue(1);
                linguisticVariableGroupEntry.getLinguisticVariableGroup().forEach((entryKey, entryValue) -> {
                    float valueTerm = entryValue.getTerms().get(termValue.getTermName()).getuValue();
                    if(termValue.getuValue() > valueTerm)
                        termValue.setuValue(valueTerm);
                });
                System.out.println("Wartość dla termu " + termKey + " : " + termValue.getuValue());
                termValue.fuzzyfication();
            });
        });
    }

    /*Agregacja*/

    public void aggregation(LinguisticVariableGroup linguisticVariableGroupEntry, LinguisticVariableGroup linguisticVariableGroupExit) {

        linguisticVariableGroupExit.getLinguisticVariableGroup().forEach((exitKey, exitValue) -> {
            System.out.println("\n" + exitValue.getLinguisticVariableName());
            exitValue.getTerms().forEach((termKey, termValue) -> {
                linguisticVariableGroupEntry.getLinguisticVariableGroup().forEach((entryKey, entryValue) -> {
                    float valueTerm = entryValue.getTerms().get(termValue.getTermName()).getuValue();
                    if(termValue.getuValue() < valueTerm)
                       termValue.setuValue(valueTerm);

                });
                System.out.println("Wartość dla termu " + termKey + " : " + termValue.getuValue());
            });

        });
    }

    /*Wyostrzanie*/

    public void defuzzyficationSingleton(FuzzyLogic fuzzyLogic, LinguisticVariableGroup linguisticVariableGroupExits) {

        float licznik = 0;
        float mianownik = 0;
        float power = 0;
        for (Map.Entry <String, LinguisticVariable> linguisticVariableEntry : linguisticVariableGroupExits.getLinguisticVariableGroup().entrySet()) {
            LinguisticVariable exitsValue = linguisticVariableEntry.getValue();
            for(Map.Entry<String, Term> termEntry : fuzzyLogic.getLinguisticVariableMap().get(exitsValue.getLinguisticVariableName()).getTerms().entrySet()) {
                Term value = termEntry.getValue();
                licznik = licznik + value.getuValue() * value.getTermBegin();
                mianownik = mianownik + value.getuValue();
            }
            if(mianownik != 0) {
                power = (licznik/mianownik);
            }
            System.out.println("\nWartość dla " + linguisticVariableEntry.getKey() + " : " + power);
        }
    }

    public void defuzzyficationIteration(FuzzyLogic fuzzyLogic, LinguisticVariableGroup linguisticVariableGroupExits, int iterationBegin, int iterationEvery, int iterationEnd) {

        float licznik = 0;
        float mianownik = 0;
        float power = 0;
        for (Map.Entry <String, LinguisticVariable> linguisticVariableEntry : linguisticVariableGroupExits.getLinguisticVariableGroup().entrySet()) {
            LinguisticVariable exitsValue = linguisticVariableEntry.getValue();
            for(Map.Entry<String, Term> termEntry : fuzzyLogic.getLinguisticVariableMap().get(exitsValue.getLinguisticVariableName()).getTerms().entrySet()) {
                for(int i = iterationBegin ; i <= iterationEnd ; i+= iterationEvery) {
                    Term value = termEntry.getValue();
                    licznik = licznik + value.valueFuzzyfication(i) * i;
                    mianownik = mianownik + value.valueFuzzyfication(i);
                }
            }
            if(mianownik != 0) {
                power = (licznik/mianownik);
            }
            System.out.println("\nWartość dla " + linguisticVariableEntry.getKey() + " : " + power);
        }
    }
}
