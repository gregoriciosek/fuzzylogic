import java.util.LinkedHashMap;

public class LinguisticVariable {

    String linguisticVariableName;
    private LinkedHashMap<String, Term> terms;

    public LinguisticVariable(String linguisticVariableName){
        this.linguisticVariableName = linguisticVariableName;
        terms = new LinkedHashMap<String, Term>();
    }

    public String getLinguisticVariableName() {
        return linguisticVariableName;
    }

    public void setLinguisticVariableName(String linguisticVariableName) {
        this.linguisticVariableName = linguisticVariableName;
    }

    public LinkedHashMap<String, Term> getTerms() {
        return terms;
    }

    public void setTerms(LinkedHashMap<String, Term> terms) {
        this.terms = terms;
    }

    public void addTerm(Term term) {
        terms.put(term.getTermName(), term);
    }
}

