public class Term {

    private String termName;
    private float termBegin;
    private float termEnd;
    private float termCoreBegin;
    private float termCoreEnd;
    private float uValue;

    public float getuValue() {
        return uValue;
    }

    public void setuValue(float uValue) {
        this.uValue = uValue;
    }

    public Term(String termName, float termBegin, float termEnd, float termCoreBegin, float termCoreEnd) {
        this.termName = termName;
        this.termBegin = termBegin;
        this.termEnd = termEnd;
        this.termCoreBegin = termCoreBegin;
        this.termCoreEnd = termCoreEnd;
        this.uValue = 1;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public float getTermBegin() {
        return termBegin;
    }

    public void setTermBegin(float termBegin) {
        this.termBegin = termBegin;
    }

    public float getTermEnd() {
        return termEnd;
    }

    public void setTermEnd(float termEnd) {
        this.termEnd = termEnd;
    }

    public float getTermCoreBegin() {
        return termCoreBegin;
    }

    public void setTermCoreBegin(float termCoreBegin) {
        this.termCoreBegin = termCoreBegin;
    }

    public float getTermCoreEnd() {
        return termCoreEnd;
    }

    public float valueFuzzyfication(float valueToFuzzyfication){
        float value = 0;

        if (valueToFuzzyfication >= termBegin && valueToFuzzyfication <= termCoreBegin) {
            if (termCoreBegin == termBegin){
                if (valueToFuzzyfication == termBegin)
                    return uValue;
                else
                    return (valueToFuzzyfication - termBegin) * uValue;
            }
            else
                return ((valueToFuzzyfication - termBegin) / (termCoreBegin - termBegin)) * uValue;
        }
            else if (valueToFuzzyfication > termCoreBegin && valueToFuzzyfication < termCoreEnd)
                return uValue;
            else if (termCoreEnd == termEnd)
                return uValue;
            else if (valueToFuzzyfication >= termCoreEnd && valueToFuzzyfication <= termEnd)
                if (termEnd == termCoreEnd)
                    return (termEnd - valueToFuzzyfication) * uValue;
                else
                    return  ((termEnd - valueToFuzzyfication) / (termEnd - termCoreEnd)) * uValue;
        return value;
    }

    public float reversedValueFuzzyfication(float valueToFuzzyfication){
        float value = 0;
        if (valueToFuzzyfication >= termBegin && valueToFuzzyfication <= termCoreBegin)
            value = (valueToFuzzyfication-termBegin) / (termCoreBegin-termBegin);
        else if (valueToFuzzyfication > termCoreBegin && valueToFuzzyfication < termCoreEnd)
            value = 1;
        else if (valueToFuzzyfication >= termCoreEnd && valueToFuzzyfication <= termEnd)
            value = (termEnd - valueToFuzzyfication) / (termEnd - termCoreEnd);
        return (1 - value);
    }

    public void fuzzyfication() {
        if (uValue != 0) {
            this.termCoreBegin = this.getTermCoreBeginNewValue();
            this.termCoreEnd = this.getTermCoreEndNewValue();
        }
    }

    public void reversedFuzzyfication() {
        if (uValue != 0) {
            this.termCoreBegin = this.getReversedTermCoreBeginNewValue();
            this.termCoreEnd = this.getReversedTermCoreEndNewValue();
        }
    }

    public float getTermCoreBeginNewValue() {
        return ((this.getTermCoreBegin() - this.getTermBegin()) * this.getuValue()) + this.getTermBegin();
    }

    public float getTermCoreEndNewValue() {
        return ((this.getTermEnd() - this.getTermCoreEnd()) * (1 - this.getuValue())) + this.getTermCoreEnd();
    }

    public float getReversedTermCoreBeginNewValue() {
        return ((this.getTermCoreBegin() - this.getTermBegin()) * (1 - this.getuValue())) + this.getTermBegin();
    }

    public float getReversedTermCoreEndNewValue() {
        return ((this.getTermEnd() - this.getTermCoreEnd()) * this.getuValue()) + this.getTermCoreEnd();
    }

    @Override
    public String toString() {
        return "Term{" +
                "termName='" + termName + '\'' +
                ", termBegin=" + termBegin +
                ", termEnd=" + termEnd +
                ", termCoreBegin=" + termCoreBegin +
                ", termCoreEnd=" + termCoreEnd +
                ", uValue=" + uValue +
                '}';
    }
}
