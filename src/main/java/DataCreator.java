public class DataCreator {

    DataCreator(){}

    public void fillMyFuzzyLogic(FuzzyLogic fuzzyLogic) {
        /*
        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("temp_0"));
        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("temp_1"));
        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("temp_2"));
        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("PWM0"));

        fuzzyLogic.getLinguisticVariableMap().get("temp_0").addTerm(new TermTriangle("T0", 10, 15, 10));
        fuzzyLogic.getLinguisticVariableMap().get("temp_0").addTerm(new TermTriangle("T1", 10, 20, 15));
        fuzzyLogic.getLinguisticVariableMap().get("temp_0").addTerm(new TermTriangle("T2", 15, 25, 20));
        fuzzyLogic.getLinguisticVariableMap().get("temp_0").addTerm(new TermTriangle("T3", 20, 30, 25));
        fuzzyLogic.getLinguisticVariableMap().get("temp_0").addTerm(new TermTriangle("T4", 25, 30, 30));

        fuzzyLogic.getLinguisticVariableMap().get("temp_1").addTerm(new TermTriangle("T0", 10, 30, 10));
        fuzzyLogic.getLinguisticVariableMap().get("temp_1").addTerm(new TermTriangle("T1", 10, 50, 30));
        fuzzyLogic.getLinguisticVariableMap().get("temp_1").addTerm(new TermTriangle("T2", 30, 70, 50));
        fuzzyLogic.getLinguisticVariableMap().get("temp_1").addTerm(new TermTriangle("T3", 50, 90, 70));
        fuzzyLogic.getLinguisticVariableMap().get("temp_1").addTerm(new TermTriangle("T4", 70, 90, 90));

        fuzzyLogic.getLinguisticVariableMap().get("temp_2").addTerm(new TermTriangle("T0", 10, 30, 10));
        fuzzyLogic.getLinguisticVariableMap().get("temp_2").addTerm(new TermTriangle("T1", 10, 50, 30));
        fuzzyLogic.getLinguisticVariableMap().get("temp_2").addTerm(new TermTriangle("T2", 30, 70, 50));
        fuzzyLogic.getLinguisticVariableMap().get("temp_2").addTerm(new TermTriangle("T3", 50, 90, 70));
        fuzzyLogic.getLinguisticVariableMap().get("temp_2").addTerm(new TermTriangle("T4", 70, 90, 90));

        fuzzyLogic.getLinguisticVariableMap().get("PWM0").addTerm(new TermSingleton("T0", 0));
        fuzzyLogic.getLinguisticVariableMap().get("PWM0").addTerm(new TermSingleton("T1", 40));
        fuzzyLogic.getLinguisticVariableMap().get("PWM0").addTerm(new TermSingleton("T2", 60));
        fuzzyLogic.getLinguisticVariableMap().get("PWM0").addTerm(new TermSingleton("T3", 80));
        */

        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("cieplo"));
        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("zimno"));
        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("niecieplo"));
        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("ogrzewanieDuze"));
        fuzzyLogic.addLinguisticVariable(new LinguisticVariable("ogrzewanieDuze_2"));


        fuzzyLogic.getLinguisticVariableMap().get("cieplo").addTerm(new TermTriangle("T0", 14, 30, 22));
        fuzzyLogic.getLinguisticVariableMap().get("zimno").addTerm(new TermTriangle("T0", 14, 24, 14));
        fuzzyLogic.getLinguisticVariableMap().get("niecieplo").addTerm(new TermTriangle("T0", 14, 30, 22));
        fuzzyLogic.getLinguisticVariableMap().get("ogrzewanieDuze").addTerm(new TermTrapez("T0", 5, 10, 9, 10));
        fuzzyLogic.getLinguisticVariableMap().get("ogrzewanieDuze_2").addTerm(new TermTrapez("T0", 5, 10, 9, 10));


    }

    public void createLinguisticVariableGroups_1(FuzzyLogic fuzzyLogic, LinguisticVariableGroup linguisticVariableGroupEntry, LinguisticVariableGroup linguisticVariableGroupExit) {
        /*
        linguisticVariableGroupEntry.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("temp_0"));
        linguisticVariableGroupEntry.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("temp_1"));
        linguisticVariableGroupEntry.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("temp_2"));
        linguisticVariableGroupExit.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("PWM0"));
        */
        linguisticVariableGroupEntry.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("cieplo"));
        linguisticVariableGroupEntry.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("zimno"));
        linguisticVariableGroupExit.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("ogrzewanieDuze"));
    }
    public void createLinguisticVariableGroups_2(FuzzyLogic fuzzyLogic, LinguisticVariableGroup linguisticVariableGroupEntry, LinguisticVariableGroup linguisticVariableGroupExit) {

        linguisticVariableGroupEntry.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("niecieplo"));
        linguisticVariableGroupEntry.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("zimno"));
        linguisticVariableGroupExit.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("ogrzewanieDuze_2"));
    }

    public void createAgregationVariableGroup(FuzzyLogic fuzzyLogic, LinguisticVariableGroup linguisticVariableGroupAggregation) {
        linguisticVariableGroupAggregation.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("ogrzewanieDuze"));
        linguisticVariableGroupAggregation.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("ogrzewanieDuze_2"));
    }


    public void createExitsLinguisticVariableGroup(FuzzyLogic fuzzyLogic, LinguisticVariableGroup linguisticVariableGroupExit) {
        /*
        linguisticVariableGroupExit.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("PWM0"));
        */
        linguisticVariableGroupExit.addLinguisticVariable(fuzzyLogic.getLinguisticVariableMap().get("ogrzewanieDuze"));
    }



}
